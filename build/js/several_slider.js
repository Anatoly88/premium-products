'use strict';

if ($('.js-several-slider').length > 0) {
	$('.js-several-slider').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		variableWidth: true,
		draggable: true,
		swipeToSlide: true,
		swipe: true,

		responsive: [{
			breakpoint: 1280,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				infinite: true,
				variableWidth: false
			}
		}, {
			breakpoint: 800,
			settings: {
				dots: false,
				infinite: true,
				slidesToShow: 2,
				slidesToScroll: 1,
				variableWidth: false
			}
		}, {
			breakpoint: 480,
			settings: {
				dots: false,
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				variableWidth: false
			}
		}]
	});
	$('.js-several-slider').addClass('visible');
}
if ($('.js-severalSimilar-slider').length > 0) {
	$('.js-severalSimilar-slider').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		variableWidth: true,
		draggable: true,
		swipeToSlide: true,
		swipe: true,

		responsive: [{
			breakpoint: 1280,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				infinite: true,
				centerMode: true,
				variableWidth: true
			}
		}, {
			breakpoint: 800,
			settings: {
				dots: false,
				infinite: true,
				slidesToShow: 3,
				slidesToScroll: 1,
				variableWidth: false
			}
		}, {
			breakpoint: 480,
			settings: {
				dots: false,
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				variableWidth: false
			}
		}]
	});
	$('.js-severalSimilar-slider').addClass('visible');
}

if ($('.js-several-choise-slider').length > 0) {
	$('.js-several-choise-slider').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		variableWidth: true,
		draggable: true,
		swipeToSlide: true,
		swipe: true,
		responsive: [{
			breakpoint: 768,
			settings: {
				dots: false,
				infinite: false,
				slidesToShow: 3,
				slidesToScroll: 1,
				variableWidth: false
			}
		}]
	});
	$('.js-several-choise-slider').addClass('visible');
}

if ($(window).width() >= 768) {
	$('.js-several-choise-slider').slick('unslick');
}

//# sourceMappingURL=several_slider.js.map
