'use strict';

if ($('.js-3d-medal').length > 0) {
	var frames = SpriteSpin.sourceArray('./images/medal_animation/medal_{frame}.png', {
		frame: [1, 61],
		digits: 5
	});
	$(".js-3d-medal").spritespin({
		animate: true,
		width: 250,
		height: 300,
		source: frames,
		sizeMode: 'fit',
		sense: -1,
		responsive: true,
		renderer: 'image'
	});
}

//# sourceMappingURL=3d_model.js.map
