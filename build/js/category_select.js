'use strict';

if ($('.categorySelect').length) {
	$(document).on('click', '.js-open-select', function(e) {
		var $categorySelect = $(e.currentTarget);
		var $categorySelectList = $('.js-select-list');

		$categorySelect.add($categorySelectList).toggleClass('open');
	});

	$(document).on('click', '.js-select-category', function(e) {
		var $categoryItem = $(e.currentTarget);
		var $categoryItemInner = $categoryItem.html();
		var $categorySelect = $('.js-open-select');
		var $categorySelectList = $('.js-select-list');
		var $categorySelected = $('.js-selected-category');

		$categorySelected.html($categoryItemInner);

		$categorySelect.add($categorySelectList).removeClass('open');
	});
}

if ($('.listItems__sidebar').length) {
	$(document).on('click', '.js-select-category', function(e) {
		e.preventDefault();

		var $categoryLink = $(e.currentTarget);
		var $categoryItem = $categoryLink.parent();
		var $categoryDropdownMenu = $categoryItem.parent().find('.js-category-dropdown');

		if (!$categoryLink.hasClass('opened')) {
			$categoryLink.addClass('opened');
			$categoryLink.toggleClass('open');
			$categoryLink.closest($categoryItem).find('.js-category-dropdown').slideToggle(function() {
				$categoryLink.removeClass('opened');
			});
		}
	});
}

//# sourceMappingURL=category_select.js.map
