if ($('.categorySelect').length) {
  $(document).on('click', '.js-open-select', (e) => {
    let $categorySelect = $(e.currentTarget);
    let $categorySelectList = $('.js-select-list');

    $categorySelect
      .add($categorySelectList)
      .toggleClass('open');
  });

  $(document).on('click', '.js-select-category', (e) => {
    let $categoryItem = $(e.currentTarget);
		let $categoryItemInner = $categoryItem.html();
		let $categorySelect = $('.js-open-select');
    let $categorySelectList = $('.js-select-list');
    let $categorySelected = $('.js-selected-category');

		$categorySelected.html($categoryItemInner);

    $categorySelect
      .add($categorySelectList)
      .removeClass('open');
  });
}

if ($('.listItems__sidebar').length) {
  $(document).on('click', '.js-select-category', (e) => {
    e.preventDefault();

    let $categoryLink = $(e.currentTarget);
    let $categoryItem = $categoryLink.parent();
    let $categoryDropdownMenu = $categoryItem.parent().find('.js-category-dropdown');

    if(!$categoryLink.hasClass('opened')) {
      $categoryLink.addClass('opened');
      $categoryLink.toggleClass('open');
      $categoryLink.closest($categoryItem).find('.js-category-dropdown').slideToggle(function() {
        $categoryLink.removeClass('opened');
       });
    }
  });
}
