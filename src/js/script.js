/* global Share:false */
// @include('./libs/bowser.js')

'use strict';

let $html = $(document.documentElement);
let $body = $(document.body);

initializationElement();
validationForm();

$(document).on('validation','.js-formField input, .js-formField textarea', function(evt, valid) {
  var errorMessage = $(this).data('error-msg');
  if (valid == true) {
    $(this).attr('placeholder', '');
  } else {
    $(this).attr('placeholder', errorMessage);
  }

  if ($(this).val().length > 0) {
    $(this).addClass('active');
  } else {
    $(this).removeClass('active');
  }
});

function initializationElement() {
 	/*SUMMOSELECT*/
   if ($('.js-select').length > 0) {
		$('.js-select').each( function() {
			var $this = $(this),
				attr = $this.attr('multiple'),
				settings = {
					placeholder: $this.data('title'),
					csvDispCount: 20,
					floatWidth: 0,
					nativeOnDevice: ['Android', 'BlackBerry', 'iPhone', 'iPad', 'iPod', 'Opera Mini', 'IEMobile', 'Silk'],
				};

			if ($this.attr('multiple')) {
				settings.selectAll = true;
				settings.selectAlltext = $this.data('select');
			}

			$this.SumoSelect(settings);
		});

		$('.SumoSelect').each(function() {
			var $select = $(this),
				$wrapper = $select.find('.optWrapper'),
				$caption = $select.find('.CaptionCont');

			$caption.on('click', function(e) {
				if ($wrapper.find('.options li').length > 4) {
					$wrapper.jScrollPane({
						contentWidth: '0px',
						verticalDragMinHeight : 30,
						verticalDragMaxHeight: 30,
						mouseWheelSpeed: 50,
						animateScroll: true,
            animateDuration: 100,
          });
				}
			});
		});
	}
  /*SUMMOSELECT*/

  /*JSCROLLPANE*/
  $('.js-scrollpane').jScrollPane({
    // contentWidth: 768,
    horizontalMinHeight : 30,
    horizontalDragMaxHeight: 30,
    mouseWheelSpeed: 50,
    animateScroll: true,
    animateDuration: 100,
  });
  /*JSCROLLPANE*/

  /*FANCYBOX*/
  $(".js-fancybox").fancybox({
    openEffect: 'fade',
    closeEffect: 'fade',
    nextEffect: 'fade',
    prevEffect: 'fade',
    padding     : 0,
    margin      : 0,
    autoSize    : true,
    width       : '100%',
    height      : '100%',
    scrolling: false,
    changeFade: 'slow',
    helpers:  {
      title : { type : 'over' },
      overlay: {
        locked: false,
        speedIn    : 0,
          speedOut   : 0,
        css : {
          'background' : 'rgba(0, 0, 0, 0.5)'
        },
      },
    },
    beforeShow : function() {
      this.title =  (this.index + 1) +' ' + '<span>из</span>' + ' ' + this.group.length;
      $("body").css({'overflow-y':'hidden'});
    },
    afterShow : function() {
      $('.fancybox-close').html('<svg><use xlink:href="images/sprites.svg#close_ic"></use></svg>');
      $('#prev-fancy').html('<svg><use xlink:href="images/sprites.svg#arrow_right_ic"></use></svg>');
      $('#next-fancy').html('<svg><use xlink:href="images/sprites.svg#arrow_right_ic"></use></svg>');
    },
    afterClose: function(){
      $("body").css({'overflow-y':'visible'});
    },
    tpl: {
      error: '<p class="fancybox-error">Запрошенное содержимое не может быть загружено.<br/>Пожалуйста, повторите попытку позже.</p>',
      closeBtn: '<a title="Закрыть" class="fancybox-item fancybox-close" href="javascript:;"></a>',
      next: '<a title="Следующий" class="fancybox-nav fancybox-next arrowLink next" id="next-fancy" href="javascript:;"><span></span></a>',
      prev: '<a title="Предыдущий" class="fancybox-nav fancybox-prev arrowLink prev" id="prev-fancy" href="javascript:;"><span></span></a>'
    }
  });
  /*FANCYBOX*/

  /*TABS*/
  $(document).on('click','.js-tab-link', function(e) {
    e.preventDefault();
    var $this		= $(this),
      $thisActive = $('.js-tab-link.-active'),
      activeData = $('.js-tab-link.-active').data('text'),
      thisText	= $this.text(),
      thisData	= $this.data('text'),
      classBlock	= $this.data('href'),
      $block		= $this.closest('.js-tabsblock'),
      $tabLinkBlock = $block.find('.js-mobiletabs'),
      tabBlock	= $block.find('.js-tab');

    $('.js-tab-link').each(function(){
      $(this).removeClass('-active');
    });


    tabBlock.each(function() {
      $(this).hide().removeClass('hidden-block');
    });

    $tabLinkBlock.removeClass('visible');
    $this.addClass('-active');

    
    $('.js-tab__' + classBlock).show().addClass('fadeIn').siblings().removeClass('fadeIn hidden-block');
  });
  /*TABS*/

  /*FORCENUMERIC*/
  if ($('.js-only-numbers').length > 0) {
    $('.js-only-numbers').ForceNumericOnly();
  }
  /*FORCENUMERIC*/
  /*PHONE INPUT*/
  var ua = navigator.userAgent.toLowerCase(),
    isAndroid = ua.indexOf("android") > -1;

  if (isAndroid) {
    $('.js-phoneInput').attr('type', 'tel');
  }

  $('.js-phoneInput').mask("+7 (999) 999-9999");
  /*PHONE INPUT*/

  /*DISABLED HOVER TOUCH*/
  if ('createTouch' in document) {
    try {
      var ignore = /:hover/;
      for (var i = 0; i < document.styleSheets.length; i++) {
        var sheet = document.styleSheets[i];
        if (!sheet.cssRules) {
          continue;
        }
        for (var j = sheet.cssRules.length - 1; j >= 0; j--) {
          var rule = sheet.cssRules[j];
          if (rule.type === CSSRule.STYLE_RULE && ignore.test(rule.selectorText)) {
            sheet.deleteRule(j);
          }
        }
      }
    }
    catch (e) {
    }
  }
  /*DISABLED HOVER TOUCH*/
};

function validationForm() {
  var myLanguage = {
    errorTitle: 'Ошибка отправки формы!',
    requiredField: 'Это обязательное поле',
    requiredFields: 'Вы задали не все обязательные поля',
    badTime: 'Вы задали некорректное время',
    badEmail: 'Вы задали некорректный e-mail',
    badTelephone: 'Вы задали некорректный номер телефона',
    badSecurityAnswer: 'Вы задали некорректный ответ на секретный вопрос',
    badDate: 'Вы задали некорректную дату',
    lengthBadStart: 'Значение должно быть в диапазоне',
    lengthBadEnd: ' символов',
    lengthTooLongStart: 'Значение длинее, чем ',
    lengthTooShortStart: 'Значение меньше, чем ',
    notConfirmed: 'Введённые значения не могут быть подтверждены',
    badDomain: 'Некорректное значение домена',
    badUrl: 'Некорретный URL',
    badCustomVal: 'Введённое значение неверно',
    andSpaces: ' и пробелы ',
    badInt: 'Значение - не число',
    badSecurityNumber: 'Введённый защитный номер - неправильный',
    badUKVatAnswer: 'Некорректный UK VAT номер',
    badStrength: 'Пароль не достаточно надёжен',
    badNumberOfSelectedOptionsStart: 'Вы должны выбрать как минимум ',
    badNumberOfSelectedOptionsEnd: ' ответов',
    badAlphaNumeric: 'Значение должно содержать только числа и буквы ',
    badAlphaNumericExtra: ' и ',
    wrongFileSize: 'Загружаемый файл слишком велик (максимальный размер %s)',
    wrongFileType: 'Принимаются файлы следующих типов %s',
    groupCheckedRangeStart: 'Выберите между ',
    groupCheckedTooFewStart: 'Выберите как минимум ',
    groupCheckedTooManyStart: 'Выберите максимум из ',
    groupCheckedEnd: ' элемент(ов)',
    badCreditCard: 'Номер кредитной карты некорректен',
    badCVV: 'CVV номер некорректно',
    wrongFileDim: 'Неверные размеры графического файла,',
    imageTooTall: 'изображение не может быть уже чем',
    imageTooWide: 'изображение не может быть шире чем',
    imageTooSmall: 'изображение слишком мало',
    min: 'минимум',
    max: 'максимум',
    imageRatioNotAccepted: 'Изображение с таким соотношением сторон не принимается',
    badBrazilTelephoneAnswer: 'Введённый номер телефона неправильный',
    badBrazilCEPAnswer: 'CEP неправильный',
    badBrazilCPFAnswer: 'CPF неправильный'
  };

  $.validate({
    language: myLanguage,
    modules: 'security',
    borderColorOnError: '#d72231',
    scrollToTopOnError: false
  });
};

// Добавляем максимальную высоту или ширину в зависимости от того портетной или альбомной ориентации изображение

$(document).ready(() => {
  let images = $('.js-img');

  images.each(function() {
    let $thisImage = $(this);
    let width = $thisImage.width();
    let height = $thisImage.height();

    if (width < height) {
      $thisImage.addClass('vertical-img');
    } else {
      $thisImage.addClass('horizontal-img');
    }
  });

  const inputNumArrowMore = $('.input-number-more');
  const inputNumArrowLess = $('.input-number-less');


  inputNumArrowMore.html('<svg><use xlink:href="images/sprites.svg#arrow_right_ic"></use></svg>');
  inputNumArrowLess.html('<svg><use xlink:href="images/sprites.svg#arrow_right_ic"></use></svg>');
  $('.slick-arrow').html('<svg><use xlink:href="images/sprites.svg#arrow_right_ic"></use></svg>');
});

$(document).on('click','.js-menu-btn', (e) => {
  let $menuBtn = $(e.currentTarget);
  let $menu = $('.js-menu');

  $menuBtn
    .add($menu)
    .toggleClass('active');
});

  if ($(window).width() < 768) {
  $(document).on('click','.js-search-show', (e) => {

    let $searchBtn = $(e.currentTarget);
    let $searchInput = $('.js-search-input');
    let $searchOverlay = $('.js-search-overlay');

    setTimeout(() => {
      $searchBtn.removeAttr('type');
    }, 200);

    $searchInput.addClass('show');
    $searchOverlay.toggleClass('show');
  });

  $(document).on('click touchstart', (e) => {
    let $target = $('.mainHeader__searchBox');
    let $searchInput = $('.js-search-input');
    let $searchOverlay = $('.js-search-overlay');
    let $searchBtn = $('.js-search-show');

    if (!$target.is(e.target) && $target.has(e.target).length === 0) {
      $searchBtn.attr('type', 'button');
      $searchInput
        .add($searchOverlay)
        .removeClass('show');
    }
  });
}

if ($(window).width() > 768 && $('.wrapper').hasClass('index')) {
  $(document).on('click','.js-search-show', (e) => {

    let $searchBtn = $(e.currentTarget);
    let $searchInput = $('.js-search-input');
    let $searchOverlay = $('.js-search-overlay');

    setTimeout(() => {
      $searchBtn.removeAttr('type');
    }, 200);

    $searchInput.addClass('show');
    $searchOverlay.toggleClass('show');
  });

  $(document).on('click touchstart', (e) => {
    let $target = $('.mainHeader__searchBox');
    let $searchInput = $('.js-search-input');
    let $searchOverlay = $('.js-search-overlay');
    let $searchBtn = $('.js-search-show');

    if (!$target.is(e.target) && $target.has(e.target).length === 0) {
      $searchBtn.attr('type', 'button');
      $searchInput
        .add($searchOverlay)
        .removeClass('show');
    }
  });
}

if ($(window).width() >= 768 && !$('.wrapper').hasClass('index')) {
  let $searchBtn = $('.js-search-show');

  $searchBtn.attr('type', 'submit');
}

if ($(window).width() >= 768 && $('.wrapper').hasClass('index')) {
  let $searchBtn = $('.js-search-show');

  $searchBtn.attr('type', 'button');
}

let checkboxIcon = '<svg><use xlink:href="images/sprites.svg#checkbox_ic"></use></svg>';
let sumoSelectMultipleItem = $('.SumoSelect .multiple .options .checkbox__button');

$(document).on('click','.SumoSelect', (e) => {
  sumoSelectMultipleItem.html(checkboxIcon);
});

// === Scroll to top ===

function scrollToAnchor(href) {
	let $target = $(href);

	if (!$target.length) {
		return;
	}

	let destination = $target.offset().top;

	$('html, body').stop().animate({
		scrollTop: destination,
	}, 1100);
}

if ($('.js-scrollLink').length) {
  $('.js-scrollLink').on('click', (e) => {
      e.preventDefault();
      scrollToAnchor($(e.currentTarget).attr('href').slice(1));
  });
}

// === / Scroll to top ====

if ($('.js-accordeon').length) {
  $(document).on('click','.js-accordeon', (e) => {
    let $accordeonLink = $(e.currentTarget).parent();
    let $accordeonContent = $accordeonLink.find('.js-accordeon-content');

    if(!$accordeonLink.hasClass('opened')) {
      $accordeonLink.addClass('opened');
      $accordeonLink.toggleClass('active');
      $accordeonLink.closest($accordeonLink).find('.js-accordeon-content').slideToggle(function() {
        $accordeonLink.removeClass('opened');
      });
    }
  });
}

// === list items filter ===

$(document).ready(function() {
  const $listItemsPopupForm = $('.js-listItemsPopup form');
  const $listItemsSidebarFilter = $('.js-listItems-sidebarFilter');

  if($(window).width() >= 1280) {
    $listItemsPopupForm.appendTo($listItemsSidebarFilter);
  }

  $( window ).resize(function() {

    if($(window).width() > 1280) {
      $listItemsPopupForm.appendTo($listItemsSidebarFilter);
    } else {
      $('.js-listItems-sidebarFilter').find('form').appendTo('.listItemsPopup .popUpForm__container');
    }
  });
})

// === / list items filter ===

// === item choise select ===
$('.oneItem__customRight .oneItem__customChoisePreview').on('click', function (e) {
  e.preventDefault();
  var $this = $(this);
  var $customTooltip = $this.parent().find('.oneItem__customTooltip');

  if(!$this.hasClass('opened')) {
    $this.addClass('opened');
    $this.toggleClass('open');
    $this.parent().find('.oneItem__customChoiseSelect').toggleClass('hide');
    $customTooltip.slideToggle(function() {
      $this.removeClass('opened');
    });
  }
});

// === / item choise select ===

// === load page ===

$(() => {
  AOS.init({
    duration: 900,
    once: true,
    disable: 'mobile',
  });
  $html.addClass('is-page-loaded');
});

// === / load page ===

svg4everybody();
