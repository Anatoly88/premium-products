if ($('.oneItem__itemSlider').length > 0) {
	$('.js-one-item-slider').slick({
		dots: false,
		arrows: false,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.js-one-item-slider-nav',
	});
	$('.js-one-item-slider-nav').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		focusOnSelect: true,
		// variableWidth: true,
		asNavFor: '.js-one-item-slider',
	});
	$('.js-one-item-slider').addClass('visible');
	$('.js-one-item-slider-nav').addClass('visible');
}

