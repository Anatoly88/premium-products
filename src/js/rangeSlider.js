let $range = $('.js-range-slider'),
$from = $('.js-from'),
$to = $('.js-to'),
range,
min = 0,
max = 10000,
from,
to;

let updateValues = function () {
    $from.prop('value', from);
    $to.prop('value', to);
};

$range.ionRangeSlider({
    type: 'double',
    min: min,
    max: max,
    grid: true,
    grid_num: 2,
    hide_min_max: true,
    hide_from_to: true,
    onChange: function (data) {
        from = data.from;
        to = data.to;
        
        updateValues();
    }
});

range = $range.data('ionRangeSlider');

let updateRange = function () {
    range.update({
        from: from,
        to: to
    });
};

$from.on('change', function () {
    from = +$(this).prop('value');
    if (from < min) {
        from = min;
    }
    if (from > to) {
        from = to;
    }

    updateValues();
    updateRange();
});

$to.on('change', function () {
    to = +$(this).prop('value');
    if (to > max) {
        to = max;
    }
    if (to < from) {
        to = from;
    }

    updateValues();
    updateRange();
});
