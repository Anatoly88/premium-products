if ($('.js-once-slider').length > 0) {
	$('.js-once-slider').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
	});
	$('.js-once-slider').addClass('visible');
}
